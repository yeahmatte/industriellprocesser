#################################################################
# Config file for Case 1  					#
# Setup 4							#
# This file is intended for the simulation program		#
# written for project in the course MION40 at LTH.		#
# Each row is has three parameters each separated by a blankspace
# - start time for the shift (0 is start and time is in seconds)#
# - duration for the shift (time in seconds)			#
# - Type of counter (self or prescription)			#
# i.e. 0 3600 self						#
# this would result in a selfcare counter opening at simulation	#
# start and being open for 1 hour				#
#################################################################
0 36000 self
7200 28800 self
0 36000 prescription
3600 32400 prescription
7200 28800 prescription
21600 10800 prescription