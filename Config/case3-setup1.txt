#################################################################
# Config file for Case 3					#
# Setup 1							#
# This file is intended for the simulation program		#
# written for project in the course MION40 at LTH.		#
# Each row is has three parameters each separated by a blankspace
# - start time for the shift (0 is start and time is in seconds)#
# - duration for the shift (time in seconds)			#
# - Type of counter (self or prescription)			#
# i.e. 0 3600 self						#
# this would result in a selfcare counter opening at simulation	#
# start and being open for 1 hour				#
#################################################################
0 36000 self
0 36000 self
0 36000 prescription
0 36000 prescription
0 36000 prescription
0 36000 prescription
0 36000 prescription