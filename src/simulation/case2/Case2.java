package simulation.case2;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import simulation.core.*;
import statistics.core.Statistics;

/**
 * Main class for case 1.
 * This class is written for the project in
 * the course MION40 at LTH.
 * @author Mattias Mellhorn
 * @version 1.0
 */
public class Case2 extends Global{
	private AbstractDispatcher prescriptionDispatcher;	
	private AbstractDispatcher selfCounterDispatcher;
	private Generator generator;
	private SignalList signalList;
	
	private List<String> shifts;
	private double cost;
	private boolean singleQueue;
	
	private Statistics stats;
	
	private boolean hasSimRunned;
	
	public Case2(Scanner scan, boolean singleQueue){
		this.hasSimRunned = false;
		this.stats = new Statistics();
		shifts = new LinkedList<String>();
		this.cost = 0;
		this.singleQueue = singleQueue;
		
		//Skip past the comments
		while(scan.hasNext()){
			String nextLine = scan.nextLine();
			if(nextLine.charAt(0) != '#'){
				shifts.add(nextLine);
			}
		}	
		
	}
	
	
    public void run() {
    	//Set time to zero at the start
    	time = 0;
    	this.cost = 0;
    	
    	//Create a new signal list
    	Signal actSignal;
    	signalList = new SignalList();

    	//Self care counters
    	List<Proc> selfCareCounters = new LinkedList<Proc>();
    	selfCareCounters.add(new SelfServiceCounter("Self Care Counter 1", signalList));
    	selfCareCounters.add(new SelfServiceCounter("Self Care Counter 2", signalList));
    	
    	//Self care dispatcher
		if(this.singleQueue) {
			this.selfCounterDispatcher = new SingleQueueDispatcher("SelfCare dispatcher", new MinQueueLengthDispatcher(), selfCareCounters);		
		} else {
			this.selfCounterDispatcher = new Dispatcher("SelfCare dispatcher", new MinQueueLengthDispatcher(), selfCareCounters);
		}
		this.selfCounterDispatcher.registerSingalList(this.signalList);
	
		//Prescriptions counters
    	List<Proc> prescriptionCounters = new LinkedList<Proc>();
    	prescriptionCounters.add(new PrescriptionCounterWOPay("Prescription Counter 1", selfCounterDispatcher ,signalList));
    	prescriptionCounters.add(new PrescriptionCounterWOPay("Prescription Counter 2", selfCounterDispatcher ,signalList));
    	prescriptionCounters.add(new PrescriptionCounterWOPay("Prescription Counter 3", selfCounterDispatcher ,signalList));
    	prescriptionCounters.add(new PrescriptionCounterWOPay("Prescription Counter 4", selfCounterDispatcher ,signalList));
    	prescriptionCounters.add(new PrescriptionCounterWOPay("Prescription Counter 5", selfCounterDispatcher ,signalList));
    	
     	//Prescription counter dispatcher
		if (this.singleQueue) {
			this.prescriptionDispatcher = new SingleQueueDispatcher("Prescription Dispatcher",new MinQueueLengthDispatcher(),prescriptionCounters);
		} else {
			this.prescriptionDispatcher = new Dispatcher("Prescription Dispatcher",new MinQueueLengthDispatcher(),prescriptionCounters);
		}
		this.prescriptionDispatcher.registerSingalList(this.signalList);
		
		
    	//Initiate the generator
		this.generator = new Generator(signalList, selfCounterDispatcher, prescriptionDispatcher);
		
		//Initiate measuerments
		this.signalList.SendSignal(MEASURE, this.prescriptionDispatcher, time, null);
		this.signalList.SendSignal(MEASURE, this.selfCounterDispatcher, time, null);
		
		//Initiate first customer
    	this.signalList.SendSignal(READY, generator, time, null);
    	
		// Iniate the counter shifts
		for(String s : this.shifts) {
			StringTokenizer st = new StringTokenizer(s, " ");
			double startTime = Double.valueOf(st.nextToken());
			double endTime = startTime + Double.valueOf(st.nextToken());
			this.cost += endTime - startTime;
			String type = st.nextToken();
			if(type.compareTo("self") == 0) {
				this.signalList.SendSignal(ADDSERVICECOUNTER, this.selfCounterDispatcher, startTime, null);
				this.signalList.SendSignal(REMOVESERVICECOUNTER, this.selfCounterDispatcher, endTime, null);
			} else if( type.compareTo("prescription") == 0 ) {
				this.signalList.SendSignal(ADDSERVICECOUNTER, this.prescriptionDispatcher, startTime, null);
				this.signalList.SendSignal(REMOVESERVICECOUNTER, this.prescriptionDispatcher, endTime, null);
			}
		}
		

    	// Main loop
    	// Time < 36000 (10h * 60 min/h * 60 s/min)
    	// Simulates 8-18
    	while (time < 36000){
    		actSignal = this.signalList.FetchSignal();
    		time = actSignal.arrivalTime;
    		actSignal.destination.TreatSignal(actSignal);
    	}
    	
    	this.hasSimRunned = true;
    }
    
    // Returns the Mean service time for the system.
    public double getServiceTimeMean() {
    		
    	return stats.calculateDoubleMean(this.getAllServiceTimes());
    }
    
    public double getServiceTimeVariance() {
    	
    	return stats.calculateDoubleVariance(this.getAllServiceTimes());
    }
    
    public int getServiceTimeObservations() {
    	
    	return this.getAllServiceTimes().size();
    }
    
    public double getMeanSelfQueue() {
    	
    	return stats.calculateIntegerMean(this.selfCounterDispatcher.getAllNbrInQueue());
    }
    
    public double getMeanPrescriptionQueue() {
    	
    	return stats.calculateIntegerMean(this.prescriptionDispatcher.getAllNbrInQueue());
    }
    
    public double getSelfServiceTimeMean() {
    	return stats.calculateDoubleMean(this.selfCounterDispatcher.getAllServiceTimes());
    }
    
    public double getSelfServiceTimeVariance() {
    	return stats.calculateDoubleVariance(this.selfCounterDispatcher.getAllServiceTimes());
    }
    
    public double getPrescriptionServiceTimeMean() {
    	return stats.calculateDoubleMean(this.prescriptionDispatcher.getAllServiceTimes());
    }
    
    public double getPrescriptionServiceTimeVariance() {
    	return stats.calculateDoubleVariance(this.prescriptionDispatcher.getAllServiceTimes());
    }
    
    public Integer[][] getArrivals() {
    	return this.generator.getArrivals();
    }
    
    public double getTime() {
    	return time;
    }
    
    public double getCost() {
    	return this.cost;
    }
    		
    public List<List<Double>> getHourlyPrescriptionServiceTimes() {
    	return this.prescriptionDispatcher.getHourlyServiceTimes();
    }
    
    public List<List<Double>> getHourlySelfServiceTimes() {
    	return this.selfCounterDispatcher.getHourlyServiceTimes();
    }
    
    private List<Double> getAllServiceTimes() {
    	if(this.hasSimRunned) {
    		List<Double> serviceTimes = new LinkedList<Double>();
    		serviceTimes.addAll(this.prescriptionDispatcher.getAllServiceTimes());
    		serviceTimes.addAll(this.selfCounterDispatcher.getAllServiceTimes());
    		
    		return serviceTimes;
    	}else{
    		return new LinkedList<Double>();
    	}
    }
    
    public String getStatistics() {
    	StringBuilder sb = new StringBuilder();
    	
    	return "";
    }
}