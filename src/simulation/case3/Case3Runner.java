package simulation.case3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import simulation.core.*;
import statistics.core.Statistics;

public class Case3Runner {
	private static final String CONFIGFILEPATH = System.getProperty("user.dir") + "\\Config\\";	
	private static final Integer NBROFRUNS = 1000;

	public static void main(String[] args) throws FileNotFoundException {
		List<String> configFiles = new LinkedList<String>();

		//Config files to run
		configFiles.add("case3-setup1.txt");
		configFiles.add("case3-setup2.txt");
		configFiles.add("case3-setup3.txt");
		configFiles.add("case3-setup4.txt");
		configFiles.add("case3-setup5.txt");
		configFiles.add("case3-setup6.txt");
		configFiles.add("case3-setup7.txt");
		
		for(String currentFile : configFiles) {
			OutPutter fileWriter = new OutPutter(currentFile);
			Statistics stats = new Statistics();	
			Scanner configData = new Scanner(new File(CONFIGFILEPATH+currentFile));

			List<Double> serviceTimeMeans = new LinkedList<Double>();
			List<Double> serviceTimeVariances = new LinkedList<Double>();
			List<Double> selfServiceTime = new LinkedList<Double>();
			List<Double> selfServiceTimeVariance = new LinkedList<Double>();
			List<Double> prescriptionServiceTime = new LinkedList<Double>();
			List<Double> prescriptionServiceTimeVariance = new LinkedList<Double>();
			List<Double> queueLengthSelfMeans = new LinkedList<Double>();
			List<Double> queueLengthPrescriptionMeans = new LinkedList<Double>();
			List<Integer[][]> arrivals = new LinkedList<Integer[][]>();
			List<Double> simTimes = new LinkedList<Double>();
			List<List<Double>> hourlyPreServiceTimes = new LinkedList<List<Double>>();
			for(int i=0; i<11; i++){
				hourlyPreServiceTimes.add(new LinkedList<Double>());
			}
			List<List<Double>> hourlySelfServiceTimes = new LinkedList<List<Double>>();
			for(int i=0; i<11; i++){
				hourlySelfServiceTimes.add(new LinkedList<Double>());
			}
			
			Case3 simModel = new Case3(configData);

			for(int i=1; i<=NBROFRUNS; i++) {
				simModel.run();

				serviceTimeMeans.add(simModel.getServiceTimeMean());
				serviceTimeVariances.add(simModel.getServiceTimeVariance());
				selfServiceTime.add(simModel.getSelfServiceTimeMean());
				selfServiceTimeVariance.add(simModel.getSelfServiceTimeVariance());
				prescriptionServiceTime.add(simModel.getPrescriptionServiceTimeMean());
				prescriptionServiceTimeVariance.add(simModel.getPrescriptionServiceTimeVariance());
				queueLengthSelfMeans.add(simModel.getMeanSelfQueue());
				queueLengthPrescriptionMeans.add(simModel.getMeanPrescriptionQueue());
				arrivals.add(simModel.getArrivals());
				simTimes.add(simModel.getTime());
				int k = 0;
				for(List<Double> l : simModel.getHourlyPrescriptionServiceTimes()){
					hourlyPreServiceTimes.get(k).addAll(l);
					k++;
				}
				k = 0;
				for(List<Double> l : simModel.getHourlySelfServiceTimes()){
					hourlySelfServiceTimes.get(k).addAll(l);
					k++;
				}

			}

			double serviceTimeMean = stats.calculateDoubleMean(serviceTimeMeans);
			double serviceTimeMeanVariance = stats.calculateDoubleMeanVariance(serviceTimeVariances);
			double selfServiceTimeMean = stats.calculateDoubleMean(selfServiceTime);
			double prescriptionServiceTimeMean = stats.calculateDoubleMean(prescriptionServiceTime);
			double upperBound = stats.upperBoundForDoubleMeanNormal(serviceTimeMean, serviceTimeMeanVariance);
			double selfCounterMeanQueue = stats.calculateDoubleMean(queueLengthSelfMeans);
			double prescriptionCounterMeanQueue = stats.calculateDoubleMean(queueLengthPrescriptionMeans);
			Double[][] arrivalMeans = stats.calculateArrivalMeans(arrivals);
			
			printTableHeader(fileWriter);

			fileWriter.print(serviceTimeMean + " & " + upperBound + " & " + selfServiceTimeMean + " & " + prescriptionServiceTimeMean + " & " + selfCounterMeanQueue + " & " + prescriptionCounterMeanQueue + "\\\\" );

			printTableFooter(fileWriter);
			
			fileWriter.print(arrivalMeans[0][0] + " " + arrivalMeans[1][0] + " " + arrivalMeans[2][0]);
			fileWriter.print(arrivalMeans[0][1] + " " + arrivalMeans[1][1] + " " + arrivalMeans[2][1]);
			fileWriter.print(arrivalMeans[0][2] + " " + arrivalMeans[1][2] + " " + arrivalMeans[2][2]);
			fileWriter.print(arrivalMeans[0][3] + " " + arrivalMeans[1][3] + " " + arrivalMeans[2][3]);
			fileWriter.print(arrivalMeans[0][4] + " " + arrivalMeans[1][4] + " " + arrivalMeans[2][4]);
			fileWriter.print(arrivalMeans[0][5] + " " + arrivalMeans[1][5] + " " + arrivalMeans[2][5]);
			fileWriter.print(arrivalMeans[0][6] + " " + arrivalMeans[1][6] + " " + arrivalMeans[2][6]);
			fileWriter.print(arrivalMeans[0][7] + " " + arrivalMeans[1][7] + " " + arrivalMeans[2][7]);
			fileWriter.print(arrivalMeans[0][8] + " " + arrivalMeans[1][8] + " " + arrivalMeans[2][8]);
			fileWriter.print(arrivalMeans[0][9] + " " + arrivalMeans[1][9] + " " + arrivalMeans[2][9]);
			
			fileWriter.print("");
			fileWriter.print("Mean sim time: " + stats.calculateDoubleMean(simTimes));
			
			fileWriter.print("");
			fileWriter.print("Hourly service Times");
			for(int k=0; k<11; k++){
				fileWriter.print(k+8 + " Pre: "+stats.calculateDoubleMean(hourlyPreServiceTimes.get(k)) + "     self: " + stats.calculateDoubleMean(hourlySelfServiceTimes.get(k)));
			}
			
			
			
			OutPutter jointWriter = new OutPutter("jointTable\\"+currentFile);
			jointWriter.print(String.format("%s & %.2f & %.2f & %.2f & %.2f & %.2f & %.2f & %.2f \\\\", currentFile, serviceTimeMean, upperBound, selfServiceTimeMean, prescriptionServiceTimeMean, selfCounterMeanQueue, prescriptionCounterMeanQueue, (simModel.getCost()/(60*60))) );
			jointWriter.close();
			
			System.out.println("Sim " + currentFile +" done");
			fileWriter.close();
		}
	}


	private static void printTableHeader(OutPutter outer){
		outer.print("\\begin{table}");
		outer.print("\\caption{Caption}");
		outer.print("\\label{table:Label}");
		outer.print("\\begin{center}");
		outer.print("\\begin{tabular}{L{1cm} L{1cm} L{1cm} L{1cm} L{1cm} L{1cm}}");
		outer.print("\\textbf{Mean service time} & \\textbf{95 upperbound} & Self service Time mean & Prescription service time mean &  \\textbf{mean self queue} & \\textbf{mean prescription queue} \\\\");
		outer.print("\\hline");

	}

	private static void printTableFooter(OutPutter outer) {
		outer.print("\\end{tabular}");
		outer.print("\\end{center}");
		outer.print("\\end{table}");

		outer.print("");
	}

}
