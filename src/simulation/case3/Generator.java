package simulation.case3;

import java.util.Random;

import simulation.core.*;
/**
 * This class generates the customers and sends them to the
 * right dispatcher.
 * All the parameters for arrivaldistribution and
 * service times is contained in this class.
 * This class is written for the project in
 * the course MION40 at LTH.
 * @author Mattias Mellhorn
 * @version 1.0
 * @since 0.1 
 */
class Generator extends Proc{
	private SignalList signalList;
	private Dispatcher selfDispatcher;
	private Dispatcher prescriptionDispatcher;
	private Random rnd;
	
	private DistributionInterface payTimeDistribution;
	private DistributionInterface selfcareTimeDistribution;
	private DistributionInterface prescriptionTimeDistribution1;
	private DistributionInterface prescriptionTimeDistribution2;
	private DistributionInterface prescriptionTimeDistribution3;
	private DistributionInterface prescriptionTimeDistribution4;
	private DistributionInterface prescriptionTimeDistribution5;
	private DistributionInterface prescriptionTimeDistribution6;
	
	private Integer[] prescriptionCustomers = new Integer[10];
	private Integer[] selfcareCustomers = new Integer[10];
	private Integer[] mixCustomers = new Integer[10];
	
	
	public Generator(SignalList signalList, Dispatcher selfDispatcher, Dispatcher prescriptionDispatcher) {
		this.registerConnections(signalList, selfDispatcher, prescriptionDispatcher);
		rnd = new Random();
		this.payTimeDistribution = new LogNormalDistribution(40,0.25);
		this.selfcareTimeDistribution = new LogNormalDistribution((2*60)-40,0.25);
		
		this.prescriptionTimeDistribution1 = new LogNormalDistribution((3*60)-40,0.25);
		this.prescriptionTimeDistribution2 = new LogNormalDistribution((4.0166*60)-40,0.25);
		this.prescriptionTimeDistribution3 = new LogNormalDistribution((5.5166*60)-40,0.25);
		this.prescriptionTimeDistribution4 = new LogNormalDistribution((5.866*60)-40,0.25);
		this.prescriptionTimeDistribution5 = new LogNormalDistribution((7.5*60)-40,0.25);
		this.prescriptionTimeDistribution6 = new LogNormalDistribution((9*60)-40,0.25);
		
		for(int i=0; i<10; i++){
			this.prescriptionCustomers[i] = 0;
			this.selfcareCustomers[i] = 0;
			this.mixCustomers[i] = 0;
		}
		
	}
	
	public void registerConnections(SignalList signalList, Dispatcher selfDispatcher, Dispatcher prescriptionDispatcher){
		this.signalList = signalList;
		this.selfDispatcher = selfDispatcher;
		this.prescriptionDispatcher = prescriptionDispatcher;
		
	}
	

	public void TreatSignal(Signal x){
		switch (x.signalType){
			case READY:{
				
				Customer cust = this.getNewCustomer();
				if(cust.getNbrOfPrescriptions()>0) {
					signalList.SendSignal(ARRIVAL, this.prescriptionDispatcher, time, cust);
				} else {
					signalList.SendSignal(ARRIVAL, this.selfDispatcher, time, cust);
				}
				//if(time > 21600)
				//	System.out.println("At " + time + " next arrival in " + this.getNextArrivalTime() + " with lambda "+this.getArrivalIntensity());
				
				signalList.SendSignal(READY, this, time + this.getNextArrivalTime(), null);
			} break;
		}
	}
	
	private Customer getNewCustomer() {
		double prob = rnd.nextDouble();
		Customer cust;
		
		if(prob < this.getSelfCareProb()) {
			//Self care only customer
			cust = new Customer(0,this.payTimeDistribution.getNextValue(),0,this.selfcareTimeDistribution.getNextValue());
			this.selfcareCustomers[((int) (time/3600))]++;
			
		} else if( prob < this.getSelfCareProb()+this.getPrescriptionProb()) {
			//Prescription only customer
			int nbrOfPrescriptions = this.getNbrOfPrescriptions();
			cust = new Customer(nbrOfPrescriptions,this.payTimeDistribution.getNextValue(),this.getPrescriptionTime(nbrOfPrescriptions),0);
			this.prescriptionCustomers[((int) (time/3600))]++;
		} else {
			//Mix customer
			int nbrOfPrescriptions = this.getNbrOfPrescriptions();
			cust = new Customer(nbrOfPrescriptions,this.payTimeDistribution.getNextValue(),this.getPrescriptionTime(nbrOfPrescriptions),this.selfcareTimeDistribution.getNextValue());
			this.mixCustomers[((int) (time/3600))]++;
		}
		return cust;
	}
	
	private double getNextArrivalTime() {
		return (Math.log( 1 - rnd.nextDouble())  * (-1 * (1/this.getArrivalIntensity())));
	}
	
	private double getArrivalIntensity() {
		if(time < (1*60*60))
			return (11.5+5.5+0.8)/(60*60);
		else if(time < (2*60*60))
			return (19.0+13.3+5.3)/(60*60);
		else if(time < (3*60*60))
			return (20.5+17+5.5)/(60*60);
		else if(time < (4*60*60))
			return (24.8+15.3+6)/(60*60);
		else if(time < (5*60*60))
			return (42.3+15.3+7.5)/(60*60);
		else if(time < (6*60*60))
			return (28.3+15+6.5)/(60*60);
		else if(time < (7*60*60))
			return (25.0+18+7)/(60*60);
		else if(time < (8*60*60))
			return (28.0+19+7.5)/(60*60);
		else if(time < (9*60*60))
			return (42.8+18.8+8.3)/(60*60);
		else if(time < (10*60*60))
			return (27.8+17.3+6)/(60*60);
		else 
			return (100);
	}
	
	private double getSelfCareProb() {
		if(time < (1*60*60))
			return (11.5/(11.5+5.5+0.8));
		else if(time < (2*60*60))
			return (19.0/(19+13.3+5.3));
		else if(time < (3*60*60))
			return (20.0/(20.5+17+5.5));
		else if(time < (4*60*60))
			return (24.8/(24.8+15.3+6));
		else if(time < (5*60*60))
			return (42.3/(42.3+15.3+7.5));
		else if(time < (6*60*60))
			return (28.3/(28.3+15+6.5));
		else if(time < (7*60*60))
			return (25.0/(25+18+7));
		else if(time < (8*60*60))
			return (28.0/(28+19+7.5));
		else if(time < (9*60*60))
			return (42.8/(42.8+18.8+8.3));
		else if(time < (10*60*60))
			return (27.8/(27.8+17.3+6));
		else 
			return (0);
	}
	
	private double getPrescriptionProb() {
		if(time < (1*60*60))
			return (5.5/(11.5+5.5+0.8));
		else if(time < (2*60*60))
			return (13.3/(19+13.3+5.3));
		else if(time < (3*60*60))
			return (17.0/(20.5+17+5.5));
		else if(time < (4*60*60))
			return (15.3/(24.8+15.3+6));
		else if(time < (5*60*60))
			return (15.3/(42.3+15.3+7.5));
		else if(time < (6*60*60))
			return (15.0/(28.3+15+6.5));
		else if(time < (7*60*60))
			return (18.0/(25+18+7));
		else if(time < (8*60*60))
			return (19.0/(28+19+7.5));
		else if(time < (9*60*60))
			return (18.8/(42.8+18.8+8.3));
		else if(time < (10*60*60))
			return (17.3/(27.8+17.3+6));
		else 
			return (0);
	}
	
	private double getMixProb() {
		if(time < (1*60*60))
			return (0.8/(11.5+5.5+0.8));
		else if(time < (2*60*60))
			return (5.3/(19+13.3+5.3));
		else if(time < (3*60*60))
			return (5.5/(20.5+17+5.5));
		else if(time < (4*60*60))
			return (6.0/(24.8+15.3+6));
		else if(time < (5*60*60))
			return (7.5/(42.3+15.3+7.5));
		else if(time < (6*60*60))
			return (6.5/(28.3+15+6.5));
		else if(time < (7*60*60))
			return (7.0/(25+18+7));
		else if(time < (8*60*60))
			return (7.5/(28+19+7.5));
		else if(time < (9*60*60))
			return (8.3/(42.8+18.8+8.3));
		else if(time < (10*60*60))
			return (6.0/(27.8+17.3+6));
		else 
			return (0);
	}
	
	private int getNbrOfPrescriptions(){
		double prob = rnd.nextDouble();
		if(prob < 0.3) {
			return 1;
		} else if (prob < 0.3+0.36) {
			return 2;
		} else if (prob < 0.3+0.36+0.19) {
			return 3;
		} else if (prob < 0.3+0.36+0.19+0.05) {
			return 4;
		} else if(prob < 0.3+0.36+0.19+0.05+0.05) {
			return 5;
		} else {
			return 6;
		}
	}
	
	private double getPrescriptionTime(int nbrOfPre) {
		switch (nbrOfPre){
			case 1:{
				return this.prescriptionTimeDistribution1.getNextValue();
			}
			case 2:{
				return this.prescriptionTimeDistribution2.getNextValue();
			}
			case 3:{
				return this.prescriptionTimeDistribution3.getNextValue();
			}
			case 4:{
				return this.prescriptionTimeDistribution4.getNextValue();
			}
			case 5:{
				return this.prescriptionTimeDistribution5.getNextValue();
			}
			case 6:{
				return this.prescriptionTimeDistribution6.getNextValue();
			}
		}
		return 0;
	}
	
	public Integer[][] getArrivals() {
		Integer[][] i = new Integer[3][10];
		i[0] = this.selfcareCustomers;
		i[1] = this.prescriptionCustomers;
		i[2] = this.mixCustomers;
		
		return i;
	}
	
}