package simulation.core;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public abstract class AbstractDispatcher extends Proc implements DispatcherInterface{
	protected String name;
	protected List<Proc> openCounters;
	protected List<Proc> closedCounters;
	protected DispatchType type;
	protected SignalList signalList;
	protected Random rnd;
	
	public AbstractDispatcher(String name, DispatchType type, List<Proc> counters){
		this.name = name;
		this.type = type;
		this.closedCounters = counters;
		this.openCounters = new LinkedList<Proc>();
		for(Proc p : counters) {
			((ServiceCounter) p).registerDispatcher(this);
		}
		this.rnd = new Random();
	}

	public abstract void TreatSignal(Signal x);

	public void registerSingalList(SignalList signalList){
		this.signalList = signalList;
	}
	
	public void registerQueues(List<Proc> l){
		closedCounters = l;
		//type.registerQueues(l);
	}
	
	protected void addServiceCounter() {
		if(this.closedCounters.size()>0){
			Proc p = this.closedCounters.remove(0);
			this.openCounters.add(p);
			this.type.registerQueues(this.openCounters);
		}
	}
	
	protected void removeServiceCounter() {
		if(this.openCounters.size()>0){
			Proc p = this.openCounters.remove(this.openCounters.size()-1);
			this.closedCounters.add(p);
			this.type.registerQueues(this.openCounters);
		}
	}
	
	public List<Double> getAllServiceTimes() {
		List<Double> list = new LinkedList<Double>();
		for(Proc p : this.openCounters) {
			list.addAll( ((ServiceCounter) p).getServiceTimes() );
		}
		for(Proc p : this.closedCounters) {
			list.addAll(((ServiceCounter) p).getServiceTimes());
		}
		return list;
	}
	
	public abstract List<Integer> getAllNbrInQueue();
	
	public List<List<Double>> getHourlyServiceTimes() {
		List<List<Double>> hourlyServiceTime = new LinkedList<List<Double>>();
		
		for(int i=0; i<11; i++){
			hourlyServiceTime.add(new LinkedList<Double>());
		}
		for(Proc p : this.openCounters) {
			int i = 0;
			
			for(List<Double> l : ((ServiceCounter) p).getHourlyServiceTime()) {
				hourlyServiceTime.get(i).addAll( l );
				i++;
			}
		}
		
		for(Proc p : this.closedCounters) {
			int i = 0;
			
			for(List<Double> l : ((ServiceCounter) p).getHourlyServiceTime()) {
				hourlyServiceTime.get(i).addAll( l );
				i++;
			}
		}
	
		return hourlyServiceTime;
		
	}
	
	protected void printOpenCounters() {
		System.out.println("Open counters:");
		for(Proc p : openCounters) {
			System.out.println("" + ((ServiceCounter) p).getName());
		}
	}
 	
}
