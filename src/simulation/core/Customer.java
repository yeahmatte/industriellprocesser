package simulation.core;

public class Customer {
	private int nbrOfPrescriptions;
	private double prescriptionServiceTime;
	private double selfServiceTime;
	private double payServiceTime;
	private double prescriptionArrivalTime;
	private double selfCounterArrivaltime;
	
	public Customer(int nbrOfPrescriptions, double payTime, double preTime, double selfTime) {
		this.nbrOfPrescriptions = nbrOfPrescriptions;
		this.payServiceTime = payTime;
		this.selfServiceTime = selfTime;
		this.prescriptionServiceTime = preTime;
	}
	
	public int getNbrOfPrescriptions() {
		return this.nbrOfPrescriptions;
	}
	
	public double getPrescriptionServiceTime() {
		return this.prescriptionServiceTime;
	}
	
	public double getSelfServiceTime() {
		return this.selfServiceTime;
	}

	public double getPayServiceTime() {
		return this.payServiceTime;
	}
	
	public double getPrescriptionCounterArrivalTime() {
		return this.prescriptionArrivalTime;
	}
	
	public double getSelfCounterArrivaltime() {
		return this.selfCounterArrivaltime;
	}
	
	public void setPrescriptionCounterArrivalTime(double arrivalTime) {
		this.prescriptionArrivalTime = arrivalTime;
	}
	
	public void setSelfCounterArrivalTime(double arrivalTime) {
		this.selfCounterArrivaltime = arrivalTime;
	}
	
}
