package simulation.core;

import java.util.List;

public interface DispatchType {
	public int nextQueue(int param);
	public void registerQueues(List<Proc> l);
}
