package simulation.core;

import java.util.LinkedList;
import java.util.List;

public class Dispatcher extends AbstractDispatcher{
	
	public Dispatcher(String name, DispatchType type, List<Proc> counters){
		super(name,type,counters);
	}

	public void TreatSignal(Signal x) {
		switch (x.signalType){
			case ARRIVAL:{
				super.signalList.SendSignal(ARRIVAL, super.openCounters.get(super.type.nextQueue(x.customer.getNbrOfPrescriptions())), time, x.customer);
			} break;
	
			case READY:{
	
			} break;
			
			case ADDSERVICECOUNTER:{
				super.addServiceCounter();
			} break;
			
			case REMOVESERVICECOUNTER:{
				super.removeServiceCounter();
			} break;
			
			case MEASURE:{
				for(Proc p : openCounters){
					signalList.SendSignal(MEASURE, p, time, null);
				}
				signalList.SendSignal(MEASURE, this, time + 5*60*rnd.nextDouble(), null);
			} break;
		}

	}
	
	@Override
	public List<Integer> getAllNbrInQueue() {
		List<Integer> list = new LinkedList<Integer>();
		for(Proc p : super.openCounters) {
			list.addAll(((ServiceCounter) p).getNbrInQueue());
		}
		for(Proc p : super.closedCounters) {
			list.addAll(((ServiceCounter) p).getNbrInQueue());
		}
		return list;
	}
	
}

