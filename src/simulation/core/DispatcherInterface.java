package simulation.core;

import java.util.List;

public interface DispatcherInterface {
	
	public void TreatSignal(Signal x);
	public void registerSingalList(SignalList signalList);
	public void registerQueues(List<Proc> l);

}
