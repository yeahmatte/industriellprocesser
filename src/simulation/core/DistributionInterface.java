package simulation.core;

public interface DistributionInterface {
	public double getNextValue();
}
