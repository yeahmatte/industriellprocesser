package simulation.core;

import java.util.List;
import java.util.Random;

public class HighQuantityDispatcher implements DispatchType {
	private List<Proc> queues;
	private int breakPoint;
	private Random rnd;
	
	public HighQuantityDispatcher(int breakPoint) {
		this.rnd = new Random();
		this.breakPoint = breakPoint;
	}
	
	public int nextQueue(int quantity) {
		if(quantity >= breakPoint)
			return 0;
		
		int nextQueue = 0;
		int min = Integer.MAX_VALUE;
		
		int i=0;
		for(Proc p : queues){
			ServiceCounter  qs = (ServiceCounter) p;
			int nbrInQueue = qs.getQueueLength();
			if(nbrInQueue < min){
				min = nbrInQueue;
				nextQueue = Integer.valueOf(i);
			}else if(nbrInQueue == min && rnd.nextBoolean()){
				nextQueue = Integer.valueOf(i);
			}
			i++;
		}
		
		return nextQueue;
	}

	public void registerQueues(List<Proc> l) {
		this.queues = l;
		
	}
}