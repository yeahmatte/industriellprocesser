package simulation.core;

import java.util.Random;

import jsc.distributions.Lognormal;

public class LogNormalDistribution implements DistributionInterface {
	private double mean;
	private double scale;
	private Lognormal dist;
	private Random rnd;
	
	public LogNormalDistribution(double mean, double scale) {
		this.mean = mean;
		this.scale = scale;
		dist = new Lognormal(Math.log(mean), scale);
		rnd = new Random();
	}
	
	public double getNextValue() {
		return dist.inverseCdf(rnd.nextDouble());
	}
	
}
