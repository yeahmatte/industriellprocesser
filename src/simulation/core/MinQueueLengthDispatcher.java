package simulation.core;

import java.util.List;
import java.util.Random;

public class MinQueueLengthDispatcher implements DispatchType {
	List<Proc> queues;
	Random rnd;
	
	public MinQueueLengthDispatcher() {
		this.rnd = new Random();
	}
	
	
	public int nextQueue(int param) {
		int nextQueue = 0;
		int min = Integer.MAX_VALUE;
		
		int i=0;
		for(Proc p : queues){
			ServiceCounter  qs = (ServiceCounter) p;
			int nbrInQueue = qs.getQueueLength();
			if(nbrInQueue < min){
				min = nbrInQueue;
				nextQueue = Integer.valueOf(i);
			}else if(nbrInQueue == min && rnd.nextBoolean()){
				nextQueue = Integer.valueOf(i);
			}
			i++;
		}
		
		return nextQueue;
	}

	public void registerQueues(List<Proc> l) {
		this.queues = l;
		
	}

}
