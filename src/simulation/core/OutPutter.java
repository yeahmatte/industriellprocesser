package simulation.core;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class OutPutter {
	private PrintWriter writer;
	private static final String OUTPUTPATH = System.getProperty("user.dir") + "\\Output\\";
	
	public OutPutter(String filename){
		try{

			writer = new PrintWriter(OUTPUTPATH+filename);

		} catch (FileNotFoundException e){
				System.out.println("Error!");
			}
	}
	
	public void print(String msg){
		writer.println(msg);
	}
	
	public void close() {
		writer.close();
	}
}
