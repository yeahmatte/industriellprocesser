package simulation.core;

public class PrescriptionCounterWOPay extends ServiceCounter {
	private Proc selfDispatcher;
	
	public PrescriptionCounterWOPay(String name, Proc selfDispatcher, SignalList signalList) {
		super(name,signalList);
		this.selfDispatcher = selfDispatcher;
	}
	
	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				super.customersInQueue.add(x.customer);
				x.customer.setPrescriptionCounterArrivalTime(time);
				if (super.getQueueLength() == 1){
						signalList.SendSignal(READY,this, time + x.customer.getPrescriptionServiceTime(),x.customer);
				}
			} break;

			case READY:{
				Customer cust = this.customersInQueue.remove(0);
				super.serviceTimes.add(time-cust.getPrescriptionCounterArrivalTime());
				super.hourlyServiceTime.get(((int) (time/3600))).add(time-cust.getPrescriptionCounterArrivalTime());
				if (selfDispatcher != null){
					signalList.SendSignal(ARRIVAL, selfDispatcher, time, cust);
				}
				
				if (super.getQueueLength() > 0){
						signalList.SendSignal(READY,this, time + super.customersInQueue.get(0).getPrescriptionServiceTime(),super.customersInQueue.get(0));
				} else {
					super.signalList.SendSignal(READY, super.dispatcher, time, null);
				}
			} break;

			case MEASURE:{
				super.nbrInQueue.add(super.getQueueLength());
			} break;
		}
	}

}
