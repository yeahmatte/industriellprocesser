package simulation.core;


public class PrescriptionCounterWPay extends ServiceCounter {
	private Proc selfDispatcher;
	
	public PrescriptionCounterWPay(String name, Proc selfDispatcher, SignalList signalList) {
		super(name,signalList);
		this.selfDispatcher = selfDispatcher;
	}
	
	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				super.customersInQueue.add(x.customer);
				x.customer.setPrescriptionCounterArrivalTime(time);
				if (super.customersInQueue.size() == 1){
					if(x.customer.getSelfServiceTime()==0){
						super.signalList.SendSignal(READY,this, time + x.customer.getPrescriptionServiceTime()+x.customer.getPayServiceTime(),x.customer);
					} else {
						super.signalList.SendSignal(READY,this, time + x.customer.getPrescriptionServiceTime(),x.customer);
					}
				}
			} break;

			case READY:{
				Customer cust = super.customersInQueue.remove(0);
				super.serviceTimes.add(time-cust.getPrescriptionCounterArrivalTime());
				super.hourlyServiceTime.get(((int) (time/3600))).add(time-cust.getPrescriptionCounterArrivalTime());
				
				if (selfDispatcher != null){
					if(cust.getSelfServiceTime()!=0)
						super.signalList.SendSignal(ARRIVAL, this.selfDispatcher, time, cust);
				}
				
				if (super.customersInQueue.size() > 0){
					if(super.customersInQueue.get(0).getSelfServiceTime()==0){
						super.signalList.SendSignal(READY,this, time + super.customersInQueue.get(0).getPrescriptionServiceTime()+super.customersInQueue.get(0).getPayServiceTime(),super.customersInQueue.get(0));
					} else {
						super.signalList.SendSignal(READY,this, time + super.customersInQueue.get(0).getPrescriptionServiceTime(),super.customersInQueue.get(0));
					}
				}else{
					super.signalList.SendSignal(READY, super.dispatcher, time, null);
				}
			} break;

			case MEASURE:{
				super.nbrInQueue.add(super.getQueueLength());
			} break;
		}
	}

}
