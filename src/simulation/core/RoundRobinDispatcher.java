package simulation.core;

import java.util.List;

public class RoundRobinDispatcher implements DispatchType {
	private int lastQueue;
	private int nbrOfQueues;
	
	public RoundRobinDispatcher() {
		this.nbrOfQueues = 0;
		this.lastQueue = nbrOfQueues-1; 
	}
	
	public int nextQueue(int param) {
		if( lastQueue == nbrOfQueues-1 ){
			lastQueue = 0;
		}else{
			lastQueue++;
		}
		
		return lastQueue;
	}

	public void registerQueues(List<Proc> l) {
		nbrOfQueues = l.size();
	}
}
