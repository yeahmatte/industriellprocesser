package simulation.core;

public class SelfServiceCounter extends ServiceCounter {
	
	public SelfServiceCounter(String name, SignalList signalList) {
		super(name,signalList);
	}
	
	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				super.customersInQueue.add(x.customer);
				x.customer.setSelfCounterArrivalTime(time);
				
				if (super.customersInQueue.size() == 1){
					
					super.signalList.SendSignal(READY, this, time + x.customer.getSelfServiceTime() + x.customer.getPayServiceTime(), x.customer);
				}
				
			} break;

			case READY:{
				Customer cust = this.customersInQueue.remove(0);
				super.serviceTimes.add(time-cust.getSelfCounterArrivaltime());
				super.hourlyServiceTime.get(((int) (time/3600))).add(time-cust.getSelfCounterArrivaltime());
				
				if (super.customersInQueue.size() > 0){
						super.signalList.SendSignal(READY,this, time + super.customersInQueue.get(0).getSelfServiceTime()+super.customersInQueue.get(0).getPayServiceTime(),super.customersInQueue.get(0));
				} else {
					super.signalList.SendSignal(READY,super.dispatcher,time,null);
				}
				
			} break;

			case MEASURE:{
				super.nbrInQueue.add(this.getQueueLength());
			} break;
		}
	}
}
