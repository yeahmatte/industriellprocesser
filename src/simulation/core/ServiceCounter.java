package simulation.core;

import java.util.LinkedList;
import java.util.List;

public abstract class ServiceCounter extends Proc {
	protected String counterName;
	protected SignalList signalList;
	protected List<Double> serviceTimes;
	protected List<Integer> nbrInQueue;
	protected List<Customer> customersInQueue;
	protected List<List<Double>> hourlyServiceTime;
	protected Proc dispatcher;
	
	public ServiceCounter(String name, SignalList signalList) {
		this.dispatcher = null;
		this.counterName = name;
		this.signalList = signalList;
		this.serviceTimes = new LinkedList<Double>();
		this.customersInQueue = new LinkedList<Customer>();
		this.nbrInQueue = new LinkedList<Integer>();
		this.hourlyServiceTime = new LinkedList<List<Double>>();
		for(int i=0; i<11; i++){
			this.hourlyServiceTime.add(new LinkedList<Double>());
		}
	}
	
	public int getQueueLength() {
		return this.customersInQueue.size();
	}
	
	public String getName() {
		return this.counterName;
	}
	
	public List<Double> getServiceTimes() {
		return this.serviceTimes;
	}
	
	public List<Integer> getNbrInQueue() {
		return this.nbrInQueue;
	}
	
	public List<List<Double>> getHourlyServiceTime() {
		return this.hourlyServiceTime;
	}
	
	public void registerDispatcher(Proc d){
		this.dispatcher = d;
	}
}
