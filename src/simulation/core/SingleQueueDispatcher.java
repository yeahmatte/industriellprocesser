package simulation.core;

import java.util.LinkedList;
import java.util.List;

public class SingleQueueDispatcher extends AbstractDispatcher{
	protected List<Integer> nbrInQueue;
	protected List<Customer> customersInQueue;
		
	public SingleQueueDispatcher(String name, DispatchType type, List<Proc> counters){
		super(name,type,counters);
		this.customersInQueue = new LinkedList<Customer>();
		this.nbrInQueue = new LinkedList<Integer>();
	}

	public void TreatSignal(Signal x) {
		switch (x.signalType){
			case ARRIVAL:{
				this.customersInQueue.add(x.customer);
				if(this.customersInQueue.size() == 1) {
					for(Proc p : super.openCounters){
						ServiceCounter  qs = (ServiceCounter) p;
						if( qs.getQueueLength() == 0)
							break;
					}
					this.customersInQueue.remove(x.customer);
					super.signalList.SendSignal(ARRIVAL, super.openCounters.get(super.type.nextQueue(x.customer.getNbrOfPrescriptions())), time, x.customer);
				}
			} break;
	
			case READY:{
				if(this.customersInQueue.size() > 0) {
					Customer cust = this.customersInQueue.remove(0);
					super.signalList.SendSignal(ARRIVAL, super.openCounters.get(super.type.nextQueue(cust.getNbrOfPrescriptions())), time, cust);
				}
			} break;
			
			case ADDSERVICECOUNTER:{
				super.addServiceCounter();
			} break;
			
			case REMOVESERVICECOUNTER:{
				super.removeServiceCounter();
			} break;
			
			case MEASURE:{
				for(Proc p : openCounters){
					super.signalList.SendSignal(MEASURE, p, time, null);
				}
				super.signalList.SendSignal(MEASURE, this, time + 5*60*rnd.nextDouble(), null);
				this.nbrInQueue.add(this.customersInQueue.size());
			} break;
		}

	}
	

	@Override
	public List<Integer> getAllNbrInQueue() {
		
		return this.nbrInQueue;
	}
	
}
