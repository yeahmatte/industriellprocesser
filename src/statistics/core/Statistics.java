package statistics.core;

import java.util.List;

public class Statistics {

	
	public double calculateDoubleMean(List<Double> values) {
		double value = 0;
		
		for(Double v : values) {
			value += v;
		}
		
		return (value/values.size());
	}
	
	public double calculateIntegerMean(List<Integer> values) {
		double value = 0;
		
		for(Integer v : values) {
			value += v;
		}
		
		return (value/values.size());
	}
	
	public double calculateDoubleVariance(List<Double> values) {
		double RSS = 0;
		double mean = this.calculateDoubleMean(values);
		
		for(Double v : values) {
			RSS += Math.pow(v-mean, 2);
		}
		
		return RSS/(values.size()-1);
	}
	
	public double calculateDoubleMeanVariance(List<Double> values) {
		double sum = 0;
		
		for(Double v : values) {
			sum += v;
		}
		
		return (sum/Math.pow(values.size(), 2));
	}
	
	//Calculates the upper bound in a one-sided confidence interval with alpha=0.05 and
	//where the values follows a normal distribution
	public double upperBoundForDoubleMeanNormal(double mean, double variance) {
		double quantile = 1.6449;
		double std = Math.sqrt(variance);
		
		return (mean + (std * quantile));
	}
	
	public Double[][] calculateArrivalMeans(List<Integer[][]> values ) {
		Double[][] r = new Double[3][10];
		
		for(int i = 0; i<10; i++){
			r[0][i] = 0.0;
			r[1][i] = 0.0;
			r[2][i] = 0.0;
		}
		
		
		for(Integer[][] run : values) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 10; j++) {
					r[i][j] += run[i][j];
				}
			}
		}
		
		for(int i = 0; i<10; i++) {
			r[0][i] = r[0][i]/values.size();
			r[1][i] = r[1][i]/values.size();
			r[2][i] = r[2][i]/values.size();
		}
		
		
		return r;
	}
}
